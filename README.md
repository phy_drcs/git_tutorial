# How to start the tutorial

Just click on the button below:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.io-warnemuende.de%2Fphy_drcs%2Fgit_tutorial/master?filepath=git_start_v2.ipynb)


# After the tutorial ...

## nach dem Ende des Jupyter-Notebook-Tutorials (ausschließlich lokales 
Repo behandelt)

- beende interaktiven Teil


## Anlegen eines Remote-Repos, hinzufügen eines Remote Servers, Pushen 
(Schnittstelle zwischen lokalem Repo und Server)

- der präsentierende erzeugt leeres Repo auf dem Gitea-Server

- den erscheinenden Hinweisen wird lokal im Terminal gefolgt (Man könnte 
hier noch den Hinweis geben, dass der remote einen beliebigen Namen 
haben kann und nicht unbedingt origin heißen muss, sondern z.B. gitea 
oder git-iow)

- das lokale Repo kann nun gepusht werden

- vielleicht auch noch in der GUI zeigen, dass nun eine Remote 
hinzugekommen ist (von hier vielleicht auch noch Tags pushen)


## Arbeiten mit gitea (Features des Servers)

- Kallaborateure hinzufügen

- Issues erzeugen, zuweisen, schließen

- lokal auf Feature-Branch wechseln, Änderungen vornehmen, 
FEature-Branch pushen

- Pull-Request erzeugen und zuweisen, bearbeiten und schließen (mergen)

- Wiki erwähnen